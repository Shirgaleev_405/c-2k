﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQpalindrom
{
    class Program
    {
        static void Main(string[] args)
        {
            var pal = Enumerable.Range(100, 900)
                .Where(x => (Enumerable.Range(x, 1000 - x).Where(y => (x * y).IsPalindrom())).Any())
                .Select(x => (Enumerable.Range(x, 1000 - x).Where(y => (x * y).IsPalindrom()).Select(y => x * y)).Max()).Max();

            var palq =
                (from a in Enumerable.Range(100, 900)
                 from b in Enumerable.Range(a, 1000 - a)
                 where (a * b).IsPalindrom()
                 select (a * b)).Max();

            Console.WriteLine(pal);
            Console.WriteLine(palq);
            Console.ReadKey();
        }

    }

        public static class Palindrom
        {
            public static bool IsPalindrom(this int num)
            {
                int temp = num;
                int rev = 0;

                while (temp > 0)
                {
                    rev = rev * 10 + temp % 10;
                    temp /= 10;
                }


                return (rev == num) ? true : false;
            }
        }
    
}

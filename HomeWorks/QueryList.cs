using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.IO;




namespace QueryLinq
{
    class Program
    {
        static void Process(string input, string xmlOutput)
        {
            using (var sreader = new StreamReader(new FileStream(input, FileMode.Open)))
            {
                var songArray = new XElement("Songs");
                while (!sreader.EndOfStream)
                {
                    var song = new XElement("Song");

                    var midle = sreader.ReadLine()?.Split('.', '-');

                    if (midle != null && midle.Length == 3)
                    {
                        song.Add(new XAttribute("Id", midle[0]));
                        song.Add(new XAttribute("Artist", midle[2].Trim(' ')));
                        song.Add(new XAttribute("Name", midle[1].Trim(' ')));

                        songArray.Add(song);
                    }
                }

                var xDoc = new XDocument();
                xDoc.Add(songArray);
                xDoc.Save(xmlOutput);
            }
        }



        static void ToString(string xmlInput, string output)
        {
            var XDoc = XDocument.Load(xmlInput);
            using (var swriter = new StreamWriter(output))
            {
                foreach (var elem in XDoc.Root?.Elements())
                {
                    swriter.WriteLine($"{elem.Name} {elem.Value}");
                    swriter.WriteLine("Attributes");
                    foreach (var attr in elem.Attributes())
                        swriter.WriteLine($"{attr.Name} {attr.Value}");

                    swriter.WriteLine("Elements");
                    foreach (var e in elem.Attributes())
                        swriter.WriteLine($"{e.Name} {e.Value}");
                }
            }
        }


        static void Xml(string input, string xmlOutput)
        {
            using (var sreader = new StreamReader(new FileStream(input, FileMode.Open)))
            {
                var artistArray = new XElement("Artists");
                while (!sreader.EndOfStream)
                {
                    var artist = new XElement("Artist");
                    // ? - если null, то не возовется
                    var midle = sreader.ReadLine()?.Split('.', '-');

                    if (midle != null && midle.Length == 3)
                    {
                        artist.Add(new XAttribute("Id", midle[0]));
                        artist.Add(new XAttribute("Name", midle[2].Trim(' ')));
                        artist.Add(new XAttribute("Song", midle[1].Trim(' ')));

                        artistArray.Add(artist);
                    }
                }

                var XDoc = new XDocument();
                XDoc.Add(artistArray);
                XDoc.Save(xmlOutput);
            }
        }





        static void Main(string[] args)
        {
            Process("input.txt", "output.xml");
            ToString("output.xml", "output.txt");

            Xml("input.txt", "task3.xml");

            var songArray = XDocument.Load("output.xml");

            var deepPurple = songArray.Root.Elements()
               .Where(x => x.Attribute("Artist").Value == "Deep Purple")
               .OrderBy(x => x.Attribute("Name").Value) //.ThenBy()
               .Select(x => $"{x.Attribute("Id").Value}. {x.Attribute("Name").Value} - {x.Attribute("Artist").Value}");


            var deepPurple2 = songArray.Root.Elements()
              .GroupBy(x => x.Attribute("Artist").Value)
              .OrderBy(x => x.Key);


            var deepPurple3 = songArray.Root.Elements()
              .OrderBy(x => x.Attribute("Artist").Value)
              .ThenBy(x => x.Attribute("Name").Value)
              .Select(x => $"{x.Attribute("Id").Value}. {x.Attribute("Name").Value} - {x.Attribute("Artist").Value}");



            var deepPurple4 = (from s in songArray.Root.Descendants()
                      where s.Attribute("Artist").Value == "Deep Purple"
                      orderby s.Attribute("Name").Value
                      select $"{s.Attribute("Id").Value}. {s.Attribute("Name").Value} - {s.Attribute("Artist").Value}"
                     );



            foreach (var elem in deepPurple)
                Console.WriteLine(elem);


            var ledZeppelin = songArray.Root.Descendants()  //.Elements()
                .Where(x => x.Attribute("Artist").Value == "Led Zeppelin");
            ledZeppelin.Remove();

            songArray.Save("noLedZeppelin.xml");

            Console.WriteLine();
            Console.WriteLine("*** Artists without duplicates");

            var noduplics = songArray.Root.Descendants().Select(x => $"{x.Attribute("Artist").Value}").Distinct();

            var noduplics1 = songArray.Root.Descendants().GroupBy(x => $"{x.Attribute("Artist").Value}").Select(x => x.Key);


            foreach (var i in noduplics1)
                Console.WriteLine(i);
            Console.WriteLine("**********");

            foreach (var i in noduplics)
                Console.WriteLine(i);


            Console.WriteLine();
            Console.WriteLine("*** Artist with max soungs' count");

            var artistWithMaxSound = songArray.Root.Descendants()
                  .GroupBy(x => $"{x.Attribute("Artist").Value}")
                  .Select(x => new { Name = x.Key, Songs = x.Count() })
                  .Aggregate((acc, x) => acc.Songs > x.Songs ? acc : x);

            Console.WriteLine(artistWithMaxSound);




            Console.WriteLine();
            Console.WriteLine("*** Sorting by songs' count");

            var sortingBySongs = songArray.Root.Descendants()
                .GroupBy(x => x.Attribute("Artist").Value)
                  .Select(x => new { Name = x.Key, Songs = x.Count() })
                  .OrderByDescending(x => x.Songs)
                  .ThenBy(x => x.Name);

            var sortingBySongs_q = from s in songArray.Root.Descendants()
                                        group songArray by s.Attribute("Artist").Value into g
                                        orderby g.Count() descending, g.Key
                                        select new { Name = g.Key, Songs = g.Count() };

            var sortingBySongs_q1 = from s in songArray.Root.Descendants()
                                         group songArray by s.Attribute("Artist").Value into g
                                         orderby g.Count() descending, g.Key
                                         select new { Name = g.Key, Songs = g.Count() };


            foreach (var elem in sortingBySongs)
                Console.WriteLine(elem);
            Console.WriteLine("**********");

            foreach (var elem in sortingBySongs_q)
                Console.WriteLine(elem);



            //3. Все песни, названия которых содержат подстроку "Rock"
            Console.WriteLine();
            Console.WriteLine("*** Songs that contains 'Rock'");

            var songsStringRock = songArray.Root.Descendants()
                .Where(x => x.Attribute("Name").Value.Contains("Rock"))
                .Select(x => $"{x.Attribute("Id").Value}. {x.Attribute("Name").Value}");

            var songsStringRock_q = (from s in songArray.Root.Descendants()
                                     where s.Attribute("Name").Value.Contains("Rock")
                                     orderby s.Attribute("Name").Value
                                     select $"{s.Attribute("Id").Value}. {s.Attribute("Name").Value}");

            foreach (var i in songsStringRock)
                Console.WriteLine(i);
            Console.WriteLine("**********");

            foreach (var i in songsStringRock_q)
                Console.WriteLine(i);



            Console.WriteLine();
            Console.WriteLine("*** Artists with songs that contain 'Rock'");


            var q1 = songArray.Root.Descendants()
                .GroupBy(x => x.Attribute("Artist").Value)
                .Select(x => new
                {
                    Name = x.Key,
                    Rock = x.Count(y => y.Attribute("NAme")
                      .Value.Contains("Rock"))
                }).Where(x => x.Rock > 0);


            var q2 = songArray.Root.Descendants()
             .GroupBy(x => x.Attribute("Artist").Value)
             .Where(x => x.Count(y => y.Attribute("Name").Value.Contains("Rock")) > 0)
             .Select(x => x.Key);

            foreach (var elem in q2)
                Console.WriteLine(elem);

            var names1 = songArray.Root.Descendants("Song");

            var names2 = songArray.Root.Descendants().Attributes().Where(x => x.Name == "Name");

            Console.ReadKey();
        }
    }
}
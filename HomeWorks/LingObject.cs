using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;


namespace LinqObjects
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***5***");
            Console.WriteLine(task());
            Console.WriteLine(task_LINQ());
            Console.WriteLine();

            Console.WriteLine("***9***");
            Console.WriteLine(task1());
            Console.WriteLine(task1_LINQ());
            Console.WriteLine();

            Console.WriteLine("***21***");
            Console.WriteLine(task3());
            Console.WriteLine();

            Console.WriteLine("***30***");
            Console.WriteLine(task2());
            Console.WriteLine(task30_LINQ());
            Console.WriteLine();

            Console.ReadKey();
        }

        public static int Nod(int a, int b)
        {
            while (a > 0 & b > 0)

                if (a > b)
                    a %= b;
                else b %= a;
            return a > 0 ? a : b;

        }


        public static int Nok(int a, int b)
        {
            return a / Nod(a, b) * b;
        }


        public static int task()
        {
            int num = 1;
            for (int i = 2; i < 20; i++)
            {
                num = Nok(num, i);
            }
            return num;
        }


        public static int task_LINQ()
        {
            return Enumerable.Range(1, 19).Aggregate((acc, x) => acc = Nok(acc, x));
        }


        public static long task1()
        {
            int n = 0;
            for (int i = 0; i <= 1000; i++)
                for (int j = i + 1; j < 1000 - i; j++)
                    if ((i < j && j < 1000 - i - j) && (i * i + j * j == (1000 - i - j) * (1000 - i - j)))
                        n = i * j * (1000 - i - j);
            return n;
        }


        public static int task1_LINQ()
        {
            return (from a in Enumerable.Range(0, 999)
                    from b in Enumerable.Range(a, 1000 - a)
                    let c = 1000 - b - a
                    where a * a + b * b == c * c && c > b
                    select a * b * c).First();
        }


        public static long task2()
        {
            int q = 0;
            for (int a = 2; a < 355000; a++)
            {
                var num = a.ToString().Select(b => b - '0').ToArray();
                int summ = num[0];
                int r = (int)Math.Pow(num[0], 5);
                for (int i = 0; i < num.Length; i++)
                {
                    summ += num[i];
                    r *= (int)Math.Pow(num[i], 5);
                }

                if (summ == a)
                    q++;
            }
            return q;
        }


        public static long task2_LINQ()
        {
            return Enumerable.Range(2, 355000).Where(a => a.ToString().Select(b => b - '0')
                         .Sum(b => (b * b * b * b * b)) == a)
                         .Sum();
        }



        public static int task3()
        {
            int r = 0;
            var dic = Enumerable.Range(1, 10000)
                .Select(x => (new { key = x, value = summ(x) }))
                .ToDictionary(x => x.key, y => y.value);
            for (int i = 1; i < 10000; i++)
            {
                for (int j = i + 1; j < 10000; j++)
                {
                    if (dic[i] == j && dic[j] == i)
                    {
                        r += i + j;
                    }
                }
            }
            return r;
        }


        public static int summ(int n)
        {
            int summ = 1;
            for (int i = 2; i * i <= n + 1; i++)
            {
                if (n % i == 0)
                {
                    summ += (n / i == i) ? i : i + n / i;
                }
            }
            return summ;
        }
    }
}
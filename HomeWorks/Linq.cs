using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace LinqCheckExcessiveNumbers
{
    public static class Extensions
    {
        public static IEnumerable<T> Filter<T>(this IEnumerable<T> source, Func<T, bool> predicate)
        {
            foreach (var elem in source)
            {
                if (predicate(elem))
                    yield return elem;
            }
        }


        public static bool IsExcesNum(this int number)
        {
            int summ = 0;
            for (int i = 1; i <= number / 2; i++)
            {
                if (number % i == 0)
                    summ += i;
            }

            if (summ > number)
                return true;
            else
                return false;
        }

    }


    public class ExcesNums : IEnumerable<int>
    {
        private class ExcesNumEnumerator : IEnumerator<int>
        {
            private int current;
            public void Dispose() { }

            public bool MoveNext()
            {
                if (current < int.MaxValue)
                {
                    current++;
                    while (!current.IsExcesNum())
                        current++;

                    return true;
                }
                else
                {
                    return false;
                }
            }

            public void Reset()
            {
                current = 0;
            }

            public int Current { get { return current; } }

            object IEnumerator.Current
            {
                get { return Current; }
            }
        }

        private ExcesNumEnumerator enumerator = new ExcesNumEnumerator();

        public IEnumerator<int> GetEnumerator()
        {
            return enumerator;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }



    public class Program
    {
        static IEnumerable<int> GetExcesNums(int count = int.MaxValue)
        {
            yield return 12;
            var s = 1;
            for (var q = 18; s < count; q++)
                if (q.IsExcesNum())
                {
                    s++;
                    yield return q;
                }
            yield break;
        }



        static void Main(string[] args)
        {
            var excesNums1 = GetExcesNums().Where(number => number == 12 || Enumerable.Range(12, 150).Any(item => number % item != 0)).Take(10);

            foreach (var num in excesNums1)
                Console.Write(num + " ");
            Console.WriteLine();


            var excesNums2 = GetExcesNums().Filter(x => x < 100);

            foreach (var num in excesNums2)
                Console.Write(num + " ");
            Console.WriteLine();


            Console.ReadKey();
        }
    }
}
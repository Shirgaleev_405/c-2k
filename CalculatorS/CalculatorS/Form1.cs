﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalculatorS
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public double firstNumber = 0;
        public double secondNumber = 0;
        public double res;
        int action;

        Boolean secondTouch = false;

        private void one_Click(object sender, EventArgs e)
        {
            if (secondTouch == false)
            {
                firstNumber = firstNumber * 10 + 1;
            }
            else
            {
                secondNumber = secondNumber * 10 + 1;
            }
        }

        private void two_Click(object sender, EventArgs e)
        {
            if (secondTouch == false)
            {
                firstNumber = firstNumber * 10 + 2;
            }
            else
            {
                secondNumber = secondNumber * 10 + 2;
            }
        }

        private void three_Click(object sender, EventArgs e)
        {
            if (secondTouch == false)
            {
                firstNumber = firstNumber * 10 + 3;
            }
            else
            {
                secondNumber = secondNumber * 10 + 3;
            }
        }

        private void four_Click(object sender, EventArgs e)
        {
            if (secondTouch == false)
            {
                firstNumber = firstNumber * 10 + 4;
            }
            else
            {
                secondNumber = secondNumber * 10 + 4;
            }
        }



        private void five_Click(object sender, EventArgs e)
        {
            if (secondTouch == false)
            {
                firstNumber = firstNumber * 10 + 5;
            }
            else
            {
                secondNumber = secondNumber * 10 + 5;
            }
        }

        private void six_Click(object sender, EventArgs e)
        {
            if (secondTouch == false)
            {
                firstNumber = firstNumber * 10 + 6;
            }
            else
            {
                secondNumber = secondNumber * 10 + 6;
            }
        }

        private void seven_Click(object sender, EventArgs e)
        {
            if (secondTouch == false)
            {
                firstNumber = firstNumber * 10 + 7;
            }
            else
            {
                secondNumber = secondNumber * 10 + 7;
            }
        }

        private void eight_Click(object sender, EventArgs e)
        {
            if (secondTouch == false)
            {
                firstNumber = firstNumber * 10 + 8;
            }
            else
            {
                secondNumber = secondNumber * 10 + 8;
            }
        }

        private void nine_Click(object sender, EventArgs e)
        {
            if (secondTouch == false)
            {
                firstNumber = firstNumber * 10 + 9;
            }
            else
            {
                secondNumber = secondNumber * 10 + 9;
            }
        }




        private void zero_Click(object sender, EventArgs e)
        {
            if (secondTouch == false)
            {
                firstNumber = firstNumber * 10;
            }
            else
            {
                secondNumber = secondNumber * 10;
            }
        }

        private void plusMinus_Click(object sender, EventArgs e)
        {
            if (secondTouch == false)
            {
                firstNumber = firstNumber * (-1);
            }
            else
            {
                secondNumber = secondNumber * (-1);
            }
        }

        private void difference_Click(object sender, EventArgs e)
        {
            action = 1;


        }

        private void additiom_Click(object sender, EventArgs e)
        {
            action = 2;

        }

        private void multiplication_Click(object sender, EventArgs e)
        {
            action = 3;
        }

        private void division_Click(object sender, EventArgs e)
        {
            action = 4;
        }

        private void equal_Click(object sender, EventArgs e)
        {
            if (action == 1)
            {
                result.Text = Convert.ToString(dif(firstNumber, secondNumber));
            }
            if (action == 2)
            {
                result.Text = Convert.ToString(add(firstNumber, secondNumber));
            }
            if (action == 3)
            {
                result.Text = Convert.ToString(mult(firstNumber, secondNumber));
            }
            if (action == 4)
            {
                result.Text = Convert.ToString(div(firstNumber, secondNumber));
            }
        }

        public static double add(double a, double b)
        {
            return a + b;
        }


        public static double div(double a, double b)
        {
            return a / b;
        }

        public static double mult(double a, double b)
        {
            return a * b;
        }

        public static double dif(double a, double b)
        {
            return a - b;
        }

    }
}

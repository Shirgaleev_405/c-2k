﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework
{
    class Program
    {
        static void Main(string[] args)
        {
            int first = 0, second = 0, res = 0; for (int i = 999; i > 100; i--) { for (int j = i; j > 100; j--) { int val = i * j; if (val.ToString().Equals(Reverse(val.ToString())) && val > res) { res = val; first = i; second = j; } } }
            Console.WriteLine("i = {0} j = {1} result = {2}", first, second, res); Console.ReadKey();
        }
        static String Reverse(String str)
        {
            char[] result = str.ToCharArray(); Array.Reverse(result); return new String(result);
        }
    }
}

﻿using LINQExessiveNumber;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQExessiveNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            int n =Convert.ToInt32( Console.ReadLine());
            ExcessiveNumbers ex = new ExcessiveNumbers();

            var arr = ex.FindNumsByBust(n);
            Console.WriteLine("Перебор:");
            foreach (var num in arr) Console.Write(num + " ");
            Console.WriteLine();

            Console.WriteLine("Явная реализация интерфейса:");
            var nums = ex.FindNumsByExcessClass(n);
            foreach (var num in nums) Console.Write(num + " ");
            Console.WriteLine();

            Console.WriteLine("Yield return:");
            nums = ex.FindNumsByYield().Take(n);
            foreach (var num in nums) Console.Write(num + " ");
            Console.WriteLine();

            Console.WriteLine("Range с фильтрацией:");
            nums = ex.FindNumsByRange(n);
            foreach (var num in nums) Console.Write(num + " ");
            Console.WriteLine();

            Console.ReadKey();
        }
    }
}

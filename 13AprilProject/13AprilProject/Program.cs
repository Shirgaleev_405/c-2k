﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace _13AprilProject
{
    class Program
    {
       
            static void ManualProcess(string input, string xmlOutput)
            {
                using (var sr = new StreamReader(new FileStream(input, FileMode.Open)))
                {
                    var songs = new XElement("Songs");
                    while (!sr.EndOfStream)
                    {
                        var song = new XElement("Song");
                        var tmp = sr.ReadLine()?.Split('.', '-');
                        if (tmp != null && tmp.Length == 3)
                        {
                            song.Add(new XAttribute("Id", tmp[0]));
                            song.Add(new XAttribute("Artist", tmp[2].Trim(' ')));
                            song.Add(new XAttribute("Name", tmp[1].Trim(' ')));
                            songs.Add(song);
                        }
                    }
                    var doc = new XDocument();
                    doc.Add(songs);
                    doc.Save(xmlOutput);
                }
            }

            static void Main()
            {
                ManualProcess("input.txt", "output.xml");
            }
        }
    }



    

